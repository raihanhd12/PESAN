<a name="readme-top"></a>

<div align="center">
  <!-- You are encouraged to replace this logo with your own! Otherwise you can also remove it. -->
  <img src="img/logo.png" alt="logo" width="140"  height="auto" />

  <br/>

  <h3><b>PESAN | Pengaduan Sederhana</b></h3>

</div>

<!-- TABLE OF CONTENTS -->

# 📗 Table of Contents

- [📖 About the Project](#about-project)
  - [🛠 Built With](#built-with)
    - [Laravel](#laravel)
- [💻 Getting Started](#getting-started)
  - [Setup](#setup)
  - [Install](#install)
  - [Usage](#usage)
- [👥 Authors](#authors)
- [🔭 Features](#future-features)
- [⭐️ Show your support](#support)

<!-- PROJECT DESCRIPTION -->

# 📖 PESAN | Pengaduan Sederhana <a name="about-project"></a>

> Describe your project in 1 or 2 sentences.

**PESAN | Pengaduan Sederhana** adalah sebuah platform yang dirancang untuk memfasilitasi masyarakat dalam menyampaikan keluhan, masukan, dan pengaduan terkait berbagai aspek layanan dan kondisi di suatu wilayah. Sistem ini bertujuan untuk meningkatkan transparansi, akuntabilitas, dan responsivitas pemerintah dalam menangani isu-isu yang dihadapi oleh warga, serta memperkuat hubungan antara pemerintah dan masyarakat.

Dengan menggunakan Sistem Pengaduan, warga dapat dengan mudah mengajukan pengaduan melalui situs web resmi. Mekanisme ini memberikan akses yang lebih luas dan fleksibel bagi masyarakat untuk menyampaikan masukan mereka, tanpa harus datang langsung ke kantor pemerintah..

## 🛠 Built With <a name="built-with"></a>

### Laravel <a name="laravel"></a>

> Framework berbasis bahasa pemrograman PHP yang bisa digunakan untuk membantu proses pengembangan sebuah website agar lebih maksimal. Dengan menggunakan Laravel, website yang dihasilkan akan lebih dinamis.

<details>
  <summary>CSS Framework</summary>
  <ul>
    <li><a href="https://getbootstrap.com/">Bootstrap</a></li>
  </ul>
</details>

<details>
<summary>Database</summary>
  <ul>
    <li><a href="https://www.mysql.com/">MySQL</a></li>
  </ul>
</details>

<!-- Features -->

### Features <a name="key-features"></a>

- **Pelapor
 <img src="img/landing_page.png" width="auto" height="auto" />**

- **Kirim Laporan
 <img src="img/submit_laporan.png" width="auto" height="auto" />**

- **Cek Laporan 
 <img src="img/halaman_laporan-1.png" width="auto" height="auto" /><img src="img/halaman_laporan-2.png" width="auto" height="auto" >**

- **Register Admin
 <img src="img/halaman_register.png" width="auto" height="auto" />**

- **Login Admin 
 <img src="img/halaman_login.png" width="auto" height="auto" />**

- **Dashboard 
 <img src="img/halaman_dashboard.png" width="auto" height="auto" />**

- **Report
 <img src="img/halaman_report-1.png" width="auto" height="auto" /> <img src="img/halaman_report-2.png" width="auto" height="auto" />**

- **Show & Edit Report
 <img src="img/halaman_showreport-1.png" width="auto" height="auto" /><img src="img/halaman_showreport-2.png" width="auto" height="auto" /><img src="img/halaman_showreport-3.png" width="auto" height="auto" /><img src="img/halaman_editreport-1.png" width="auto" height="auto" /><img src="img/halaman_editreport-2.png" width="auto" height="auto" /><img src="img/halaman_editreport-3.png" width="auto" height="auto" />**

- **Report Tracker
 <img src="img/halaman_tracker.png" width="auto" height="auto" /><img src="img/halaman_showtracker.png" width="auto" height="auto" />**

- **Activity Log
 <img src="img/halaman_log-1.png" width="auto" height="auto" /><img src="img/halaman_log-2.png" width="auto" height="auto" />**

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->

## 💻 Getting Started <a name="getting-started"></a>

`$ > git clone git@gitlab.com:raihanhd12/PESAN.git

`$ > cd PESAN`

`$ > composer install`

`$ > cp env.example .env`

Konfigurasi Database.

`$ > php artisan migrate:fresh --seed`

### Usage

To run the project, execute the following command:

`$ > php artisan serve`
> Jalankan xampp 


<!-- AUTHORS -->

## 👥 Authors <a name="authors"></a>

👤 **Raihan Hidayatullah Djunaedi**

- GitHub: [@raihanhd12](https://github.com/raihanhd12)
- LinkedIn: [LinkedIn](https://www.linkedin.com/in/raihanhd/)



## ⭐️ Show your support <a name="support"></a>

<a href="https://www.buymeacoffee.com/raihanhd"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" width="200" /></a>


<p align="right">(<a href="#readme-top">back to top</a>)</p>
